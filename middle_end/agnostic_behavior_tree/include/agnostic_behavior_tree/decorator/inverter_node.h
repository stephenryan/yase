/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_INVERTER_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_INVERTER_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {
// The InverterNode inverts the execution state of the child behavior
class ABT_DLL_EXPORT InverterNode : public DecoratorNode {
 public:
  InverterNode(Extension::UPtr extension_ptr = nullptr);

  virtual ~InverterNode() override = default;

 private:
  NodeStatus tick() final;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_INVERTER_NODE_H
