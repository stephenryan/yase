/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_DATA_DECLARATION_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_DATA_DECLARATION_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <memory>
#include <string>
#include <vector>

namespace yase {

// The generic DataDeclaration interface
//
// Allows to declare data
class ABT_DLL_EXPORT DataDeclaration {
 public:
  using UPtr = std::unique_ptr<DataDeclaration>;

  DataDeclaration() = default;
  virtual ~DataDeclaration() = default;
  DataDeclaration(const DataDeclaration& copy_from) = delete;
  DataDeclaration& operator=(const DataDeclaration& copy_from) = delete;
  DataDeclaration(DataDeclaration&&) = delete;
  DataDeclaration& operator=(DataDeclaration&&) = delete;

  // Declare data for the child nodes.
  virtual void lookupAndRegisterData(Blackboard& blackboard) = 0;
};

// The specific TypeDeclarer
//
// Declares data of specific type and key
template <typename DataType>
class ABT_DLL_EXPORT TypeDeclarer : public DataDeclaration {
 public:
  TypeDeclarer(const std::string& key, DataType data) : m_key(key), m_data(std::move(data)){};

  // Declare specific data type
  void lookupAndRegisterData(Blackboard& blackboard) final { blackboard.set<DataType>(m_key, m_data); };

 private:
  const std::string m_key;
  DataType m_data;
};

/// The DataDeclarationNode
//
// Allows to declare data for the sub tree in a generic way.
class ABT_DLL_EXPORT DataDeclarationNode : public DecoratorNode {
 public:
  // Ctor for named node and multiple managed declarations
  DataDeclarationNode(const std::string& name,
                      std::vector<DataDeclaration::UPtr> declarations,
                      Extension::UPtr extension_ptr = nullptr);

  // Convenience ctor for only one declaration
  DataDeclarationNode(const std::string& name,
                      DataDeclaration::UPtr declaration,
                      Extension::UPtr extension_ptr = nullptr);

  virtual ~DataDeclarationNode() override = default;

 private:
  // Ticks the decorated child node directly
  NodeStatus tick() override;

  // Register data of all managed declarations
  void lookupAndRegisterData(Blackboard& blackboard) final;

  // Helper function to push back declaration, as for unique ptrs no initializer list can be used
  static std::vector<DataDeclaration::UPtr> instantiateInContainer(DataDeclaration::UPtr managed_declaration);

  // The managed data declarations
  std::vector<DataDeclaration::UPtr> m_managed_declarations;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_DATA_DECLARATION_NODE_H
