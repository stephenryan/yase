/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_REPEAT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_REPEAT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

// Repeats a behavior n times
class ABT_DLL_EXPORT RepeatNTimesNode : public DecoratorNode {
 public:
  explicit RepeatNTimesNode(const size_t repeat_times, Extension::UPtr extension_ptr = nullptr);

  virtual ~RepeatNTimesNode() override = default;

  // onInit() is called is called directly before first evaluation - resets the behavior
  void onInit() final;

  static std::string createNodeName(const size_t repeat_times);

  size_t repeatTimes() const;

 private:
  // Waits until condition is evaluated once to true
  NodeStatus tick() final;

  // Adds execution info about current repeat state
  void addExecutionInfo();

  // Indicates if child was already indicated before
  bool m_child_initialised{false};

  // Indicates if already evaluated to true
  size_t m_counter{0};

  // Desired of successful repeats
  size_t m_desired_repeats;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_REPEAT_NODE_H
