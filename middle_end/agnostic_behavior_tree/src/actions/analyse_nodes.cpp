/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"

namespace yase {

AnalyseNode::AnalyseNode(const size_t max_repeat_ticks, const NodeStatus return_status, Extension::UPtr extension_ptr)
    : AnalyseNode("", max_repeat_ticks, return_status, std::move(extension_ptr)) {}

AnalyseNode::AnalyseNode(const std::string& name,
                         const size_t max_repeat_ticks,
                         const NodeStatus return_status,
                         Extension::UPtr extension_ptr)
    : ActionNode(std::string("Analyse[").append(name).append("]"), std::move(extension_ptr)),
      m_max_repeat_ticks(max_repeat_ticks),
      m_return_status(return_status) {}

bool AnalyseNode::isInitialised() const { return m_initialised; }

size_t AnalyseNode::overallTicks() const { return m_overall_ticks; }

size_t AnalyseNode::ticksSinceInit() const { return m_ticks_since_init; }

size_t AnalyseNode::onInitCalls() const { return m_on_init_calls; }

size_t AnalyseNode::onTerminateCalls() const { return m_on_terminate_calls; }

NodeStatus AnalyseNode::tick() {
  m_overall_ticks++;
  m_ticks_since_init++;
  if (m_repeat_counter < m_max_repeat_ticks) {
    m_repeat_counter++;
    executionInfo(std::string("Ticked ")
                      .append(std::to_string(m_repeat_counter))
                      .append(" of ")
                      .append(std::to_string(m_max_repeat_ticks))
                      .append(" times"));
    return NodeStatus::kRunning;
  }
  return m_return_status;
}

void AnalyseNode::onInit() {
  m_repeat_counter = 0;
  m_initialised = true;
  m_ticks_since_init = 0;
  m_on_init_calls++;
}

void AnalyseNode::onTerminate() {
  m_initialised = false;
  m_on_terminate_calls++;
}

AlwaysRunning::AlwaysRunning(Extension::UPtr extension_ptr)
    : AnalyseNode("AlwaysRunning", 0, NodeStatus::kRunning, std::move(extension_ptr)) {}

AlwaysSuccess::AlwaysSuccess(Extension::UPtr extension_ptr)
    : AnalyseNode("AlwaysSuccess", 0, NodeStatus::kSuccess, std::move(extension_ptr)) {}

AlwaysFailure::AlwaysFailure(Extension::UPtr extension_ptr)
    : AnalyseNode("AlwaysFailure", 0, NodeStatus::kFailure, std::move(extension_ptr)) {}

}  // namespace yase