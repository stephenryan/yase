/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/constraint_node.h"

#include <algorithm>
#include <exception>
#include <string>

namespace yase {

std::string ABT_DLL_EXPORT toStr(const EvaluationPhase& phase) {
  switch (phase) {
    case EvaluationPhase::kPre:
      return "Pre";
    case EvaluationPhase::kRuntime:
      return "Runtime";
    case EvaluationPhase::kPost:
      return "Post";
    default:
      return "UnkownEvaluationPhase";
  }
}

ConstraintNode::ConstraintNode(TimedConstraint condition, Extension::UPtr extension_ptr)
    : DecoratorNode(std::string("Constraint[")
                        .append(toStr(condition.first))
                        .append("Check[")
                        .append(condition.second->name)
                        .append("]"),
                    std::move(extension_ptr)) {
  switch (condition.first) {
    case EvaluationPhase::kPre:
      m_pre_constraint = std::move(condition.second);
      break;
    case EvaluationPhase::kRuntime:
      m_runtime_constraint = std::move(condition.second);
      break;
    case EvaluationPhase::kPost:
      m_post_constraint = std::move(condition.second);
      break;
    default:
      break;
  }
}

void ConstraintNode::onInit() { m_first_call = true; }

NodeStatus ConstraintNode::tick() {
  // Evaluate pre check
  if (m_first_call) {
    if (m_pre_constraint != nullptr) {
      if (!m_pre_constraint->evaluate()) {
        executionInfo(createErrorMsg("pre evaluation", m_pre_constraint->name));
        return NodeStatus::kFailure;
      }
    }
    m_first_call = false;
    child().onInit();
  }

  // Check runtime condition
  if (m_runtime_constraint) {
    if (!m_runtime_constraint->evaluate()) {
      executionInfo(createErrorMsg("runtime evaluation", m_runtime_constraint->name));
      return NodeStatus::kFailure;
    }
  }

  // Execute the child behavior
  NodeStatus child_status = child().executeTick();

  // If successfully finished --> post checks
  if (child_status == NodeStatus::kSuccess) {
    if (m_post_constraint) {
      if (!m_post_constraint->evaluate()) {
        executionInfo(createErrorMsg("post evaluation", m_post_constraint->name));
        return NodeStatus::kFailure;
      }
    }
  }

  // All constraints fulfilled --> return child status like there were no condition evaluation
  return child_status;
}

void ConstraintNode::lookupAndRegisterData(Blackboard& blackboard) {
  if (m_pre_constraint != nullptr) {
    m_pre_constraint->lookupAndRegisterData(blackboard);
  }
  if (m_runtime_constraint != nullptr) {
    m_runtime_constraint->lookupAndRegisterData(blackboard);
  }
  if (m_post_constraint != nullptr) {
    m_post_constraint->lookupAndRegisterData(blackboard);
  }
}

std::string ConstraintNode::createErrorMsg(const std::string& constraint_time, const std::string& constraint_info) {
  std::string error_msg = "Violation of constraint [";
  error_msg.append(constraint_info);
  error_msg.append("] at ");
  error_msg.append(constraint_time);
  error_msg.append(".");
  return error_msg;
}

}  // namespace yase
