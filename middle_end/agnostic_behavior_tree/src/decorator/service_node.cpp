/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/service_node.h"

namespace yase {

ServiceNode::ServiceNode(const std::string& name,
                         std::vector<ServiceNode::Service::UPtr> managed_services,
                         Extension::UPtr extension_ptr)
    : DecoratorNode(std::string("Service[").append(name).append("]"), std::move(extension_ptr)),
      m_managed_services(std::move(managed_services)) {
  for (const auto& service : m_managed_services) {
    if (service == nullptr) {
      std::string error_msg = "Error in ServiceNode [";
      error_msg.append(this->name()).append("]: One of the services is a nullptr!");
      throw std::invalid_argument(error_msg);
    }
  }
}

ServiceNode::ServiceNode(const std::string& name,
                         ServiceNode::Service::UPtr managed_service,
                         Extension::UPtr extension_ptr)
    : ServiceNode(name, instantiateInContainer(std::move(managed_service)), std::move(extension_ptr)) {}

void ServiceNode::onInit() {
  for (const auto& service : m_managed_services) {
    service->onInit();
  }
  DecoratorNode::onInit();
}

NodeStatus ServiceNode::tick() {
  // Update services
  for (auto& service : m_managed_services) {
    service->preUpdate();
  }
  // Tick decorated child
  NodeStatus child_status = child().executeTick();

  // Update services
  for (auto& service : m_managed_services) {
    service->postUpdate();
  }
  return child_status;
}

void ServiceNode::lookupAndRegisterData(Blackboard& blackboard) {
  for (auto& service : m_managed_services) {
    service->lookupAndRegisterData(blackboard);
  }
}

std::vector<ServiceNode::Service::UPtr> ServiceNode::instantiateInContainer(ServiceNode::Service::UPtr service) {
  std::vector<ServiceNode::Service::UPtr> new_vector;
  new_vector.push_back(std::move(service));
  return new_vector;
}

}  // namespace yase