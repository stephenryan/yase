/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/scoped_blackboard.h"

#include <gtest/gtest.h>
#include <memory>

namespace yase {

// Demonstrates how to retrieve data form blackboard
TEST(ScopedBlackboardTest, example) {
  // 1. Set up a blackboard
  BlackboardOwner::Ptr demo_blackboard = std::make_shared<BlackboardOwner>("Scope1");

  // 2. Store any kind of data in there with (unique!) keys
  demo_blackboard->set<int>("value_key", 707);
  demo_blackboard->set<std::shared_ptr<double>>("shared_ptr_key", std::make_shared<double>(2.0));

  // 3. Anyone with this (even nested) blackboard can retrieve the data again with the key.
  // Retrieving a pure value will get the user a COPY of the data.
  EXPECT_EQ(demo_blackboard->get<int>("value_key"), 707);
  // If the data is a shared_ptr, everyone getting this data will work on the SAME underlying instance
  EXPECT_DOUBLE_EQ(*(demo_blackboard->get<std::shared_ptr<double>>("shared_ptr_key")), 2.0);
}

TEST(ScopedBlackboardTest, no_depth_insert_and_exist) {
  // blackboard with no depth
  BlackboardOwner local_blackboard{"NoDepthBlackboard"};

  // insert operations
  // legal
  local_blackboard.set<std::shared_ptr<int>>("key_1", std::make_shared<int>(1));
  local_blackboard.set<std::shared_ptr<std::string>>("key_2", std::make_shared<std::string>("test_string"));
  local_blackboard.set<std::shared_ptr<double>>("key_3", std::make_shared<double>(2.0));
  // illegal - duplicate declaration not allowed
  EXPECT_THROW(local_blackboard.set<std::shared_ptr<double>>("key_1", std::make_shared<double>(2.0));
               , std::runtime_error);

  // lookup operations
  // legal
  EXPECT_EQ(*(local_blackboard.get<std::shared_ptr<int>>("key_1")), 1);
  // illegal - not existing
  EXPECT_THROW(local_blackboard.get<std::shared_ptr<int>>("key_not_existing");, std::invalid_argument);
  // illegal - wrong type
  EXPECT_THROW(local_blackboard.get<std::shared_ptr<double>>("key_1");, std::invalid_argument);

  // test if values still exist
  // legal
  EXPECT_TRUE(local_blackboard.exists("key_1"));
  EXPECT_TRUE(local_blackboard.exists<std::shared_ptr<int>>("key_1"));
  EXPECT_TRUE(local_blackboard.exists("key_2"));
  EXPECT_TRUE(local_blackboard.exists<std::shared_ptr<std::string>>("key_2"));
  // illegal - wrong type
  EXPECT_FALSE(local_blackboard.exists<std::shared_ptr<std::string>>("key_1"));
  EXPECT_FALSE(local_blackboard.exists<std::shared_ptr<double>>("key_1"));
}

TEST(ScopedBlackboardTest, depth_insert_and_exist) {
  // This works only of the are no other insert operations after a blackboard was passed as a parent!

  // ---- first scope ----
  BlackboardOwner::Ptr blackboard_scope_1 = std::make_shared<BlackboardOwner>("Scope1");
  // declare operations
  blackboard_scope_1->set<std::shared_ptr<int>>("key_1", std::make_shared<int>(1));
  // exists operations
  EXPECT_TRUE(blackboard_scope_1->exists<std::shared_ptr<int>>("key_1"));
  EXPECT_FALSE(blackboard_scope_1->exists<std::shared_ptr<int>>("wrong_type"));
  EXPECT_FALSE(blackboard_scope_1->exists<std::shared_ptr<std::string>>("key_1"));
  // lookUp operations
  EXPECT_EQ(*(blackboard_scope_1->get<std::shared_ptr<int>>("key_1")), 1);
  EXPECT_THROW(blackboard_scope_1->get<std::shared_ptr<int>>("key_not_existing");, std::invalid_argument);

  // ---- second scope ----
  BlackboardOwner::Ptr blackboard_scope_2 = std::make_shared<BlackboardOwner>("Scope2", blackboard_scope_1);
  // declare operations
  blackboard_scope_2->set<std::shared_ptr<double>>("key_2", std::make_shared<double>(2.0));
  // illegal - duplicate key
  EXPECT_THROW(blackboard_scope_2->set<std::shared_ptr<int>>("key_1", std::make_shared<int>(2));, std::runtime_error);
  EXPECT_THROW(blackboard_scope_2->set<std::shared_ptr<double>>("key_1", std::make_shared<double>(2.0));
               , std::runtime_error);
  // exists operations
  EXPECT_TRUE(blackboard_scope_2->exists<std::shared_ptr<int>>("key_1"));
  EXPECT_TRUE(blackboard_scope_2->exists<std::shared_ptr<double>>("key_2"));
  EXPECT_FALSE(blackboard_scope_2->exists<std::shared_ptr<std::string>>("key_1"));
  // lookUp operations
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<int>>("key_1")), 1);
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<double>>("key_2")), 2.0);
  EXPECT_THROW(blackboard_scope_2->get<std::shared_ptr<int>>("key_not_existing");, std::invalid_argument);

  // ---- third scope ----
  BlackboardOwner::Ptr blackboard_scope_3 = std::make_shared<BlackboardOwner>("Scope3", blackboard_scope_2);
  blackboard_scope_3->set<std::shared_ptr<char>>("key_3", std::make_shared<char>(1));
  EXPECT_TRUE(blackboard_scope_3->exists<std::shared_ptr<int>>("key_1"));
  EXPECT_TRUE(blackboard_scope_3->exists<std::shared_ptr<double>>("key_2"));
  EXPECT_TRUE(blackboard_scope_3->exists<std::shared_ptr<char>>("key_3"));

  // ---- check for consistency ----

  // illegal operations - out of scope and not visible
  // declare operations
  EXPECT_FALSE(blackboard_scope_1->exists<std::shared_ptr<double>>("key_2"));
  EXPECT_FALSE(blackboard_scope_1->exists<std::shared_ptr<char>>("key_3"));
  // lookUp operation
  EXPECT_THROW(blackboard_scope_1->get<std::shared_ptr<double>>("key_2");, std::invalid_argument);
  EXPECT_THROW(blackboard_scope_1->get<std::shared_ptr<char>>("key_3");, std::invalid_argument);
}

TEST(ScopedBlackboardTest, test_proxy_remap) {
  // FIRST SCOPE LEVEL
  BlackboardOwner::Ptr blackboard_scope_1 = std::make_shared<BlackboardOwner>("Scope1");
  blackboard_scope_1->set<std::shared_ptr<int>>("scope_1_key_1", std::make_shared<int>(1));
  blackboard_scope_1->set<std::shared_ptr<int>>("scope_1_key_2", std::make_shared<int>(2));
  blackboard_scope_1->set<std::shared_ptr<int>>("scope_1_key_3", std::make_shared<int>(3));

  // SECOND SCOPE LEVEL
  // blackboard with local variable and remap_proxy
  BlackboardOwner::Ptr blackboard_scope_2 = std::make_shared<BlackboardOwner>("Scope2", blackboard_scope_1);
  blackboard_scope_2->set<std::shared_ptr<double>>("scope_2_key_1", std::make_shared<double>(2.0));
  blackboard_scope_2->setProxyTable(
      Blackboard::ProxyTable{{"scope_2_proxy_key_1", "scope_1_key_1"}, {"scope_1_key_3", "scope_1_key_3"}});

  // legal, as in local scope
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<double>>("scope_2_key_1")), 2.0);
  // legal, key is visible, as remmaped
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<int>>("scope_2_proxy_key_1")), 1);
  // legal, key is visible, as remmaped (to same key)
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<int>>("scope_1_key_3")), 3);
  // illegal - key not visible, must use the remmaped key
  EXPECT_THROW(blackboard_scope_2->get<std::shared_ptr<int>>("scope_1_key_1");, std::invalid_argument);
  // illegal - key not visible, as not remapped at all
  EXPECT_THROW(blackboard_scope_2->get<std::shared_ptr<int>>("scope_1_key_2");, std::invalid_argument);

  // THIRD SCOPE LEVEL
  BlackboardOwner::Ptr blackboard_scope_3 = std::make_shared<BlackboardOwner>("Scope3", blackboard_scope_2);
  blackboard_scope_3->set<std::shared_ptr<std::string>>("key_3", std::make_shared<std::string>("test"));

  // legal, as key is remmaped upstream and therefore available
  EXPECT_NO_THROW(blackboard_scope_3->set<std::shared_ptr<char>>("scope_1_key_1", std::make_shared<char>(1)););
  // legal, as in local scope
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<double>>("scope_2_key_1")), 2.0);
  // legal, key is visible, as remmaped
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<int>>("scope_2_proxy_key_1")), 1);
  // legal, key is visible, as remmaped (to same key)
  EXPECT_EQ(*(blackboard_scope_2->get<std::shared_ptr<int>>("scope_1_key_3")), 3);
  // illegal - key not visible, must use the remmaped key
  EXPECT_THROW(blackboard_scope_2->get<std::shared_ptr<int>>("scope_1_key_1");, std::invalid_argument);
  // illegal - key not visible, as not remapped at all
  EXPECT_THROW(blackboard_scope_2->get<std::shared_ptr<int>>("scope_1_key_2");, std::invalid_argument);
}

TEST(ScopedBlackboardTest, depth_print) {
  // FIRST SCOPE LEVEL
  BlackboardOwner::Ptr blackboard_scope_1 = std::make_shared<BlackboardOwner>("Scope1");
  blackboard_scope_1->set<std::shared_ptr<int>>("scope_1_key_1", std::make_shared<int>(1));
  blackboard_scope_1->set<std::shared_ptr<int>>("scope_1_key_2", std::make_shared<int>(2));
  blackboard_scope_1->set<std::shared_ptr<int>>("scope_1_key_3", std::make_shared<int>(3));

  // SECOND SCOPE LEVEL
  // blackboard with local variable and remap_proxy
  BlackboardOwner::Ptr blackboard_scope_2 = std::make_shared<BlackboardOwner>("Scope2", blackboard_scope_1);
  blackboard_scope_2->set<std::shared_ptr<double>>("scope_2_key_1", std::make_shared<double>(2.0));
  blackboard_scope_2->setProxyTable(
      Blackboard::ProxyTable{{"scope_2_proxy_key_1", "scope_1_key_1"}, {"scope_1_key_3", "scope_1_key_3"}});

  // THIRD SCOPE LEVEL
  BlackboardOwner::Ptr blackboard_scope_3 = std::make_shared<BlackboardOwner>("Scope3", blackboard_scope_2);
  blackboard_scope_3->set<std::shared_ptr<int>>("key_3", std::make_shared<int>(707));
  blackboard_scope_3->set<std::shared_ptr<int>>("scope_1_key_1", std::make_shared<int>(1337));

  EXPECT_NO_THROW(blackboard_scope_3->printAllAccessibleSymbols(););
}

}  // namespace yase
