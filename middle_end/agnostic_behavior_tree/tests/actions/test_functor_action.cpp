/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/functor_action_node.h"

#include <gtest/gtest.h>

namespace yase {
TEST(ACTION_TEST, action_functor) {
  // A lambda, which succeeds after three ticks
  size_t count = 0;
  auto TestFunctor = [&count]() mutable {
    if (count >= 3) {
      return NodeStatus::kSuccess;
    }
    count++;
    return NodeStatus::kRunning;
  };

  FunctorActionNode test_node(TestFunctor);

  EXPECT_EQ(test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(test_node.executeTick(), NodeStatus::kSuccess);
}

}  // namespace yase
