/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/decorator/start_at_node.h"
#include "agnostic_behavior_tree/utils/condition.h"

#include <gtest/gtest.h>

namespace yase {

// Evaluates to true after three evaluation calls
class TrueAfterThreeEvaluationCalls : public Condition {
 public:
  explicit TrueAfterThreeEvaluationCalls() : Condition("TrueAfterThreeEvaluationCalls"){};

  void onInit() final { m_evaluation_calls = 0; }

  bool evaluate() override {
    if (m_evaluation_calls >= 3) {
      return true;
    }
    m_evaluation_calls++;
    return false;
  };

 private:
  unsigned int m_evaluation_calls{0};
};

// Test if child behavior execution is postboned until StartAt condition is met
TEST(StartAtNodeFixture, wait_until_condition_met) {
  /// -------- TEST SETUP --------------
  // └─ [Decorator::StartAT[TrueAfterThreeEvaluationCalls]]
  //     └─ [Action::AnalyseNode]
  DecoratorNode::Ptr start_at_tree_calls =
      std::make_shared<StartAtNode>(std::make_unique<TrueAfterThreeEvaluationCalls>());
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);
  start_at_tree_calls->setChild(dummy_node);

  // Initialise the whole tree and call onInit of root node
  start_at_tree_calls->distributeData();
  start_at_tree_calls->onInit();

  // Tick tree three times - after that the Wait decorator should start executing the child behavior.
  EXPECT_EQ(start_at_tree_calls->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(start_at_tree_calls->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(start_at_tree_calls->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(dummy_node->isInitialised(), false);

  // Wait finishes -> child behavior must now start
  EXPECT_EQ(start_at_tree_calls->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(dummy_node->isInitialised(), true);

  // Decorated child finishes
  EXPECT_EQ(start_at_tree_calls->executeTick(), NodeStatus::kSuccess);
  // Just double check if child behavior was terminated correctly
  start_at_tree_calls->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

}  // namespace yase
