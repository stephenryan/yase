/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/decorator/stop_at_node.h"
#include "agnostic_behavior_tree/utils/condition.h"

#include <gtest/gtest.h>

namespace yase {

// Evaluates to true after three evaluation calls
class TrueAfterThreeEvaluationCalls : public Condition {
 public:
  explicit TrueAfterThreeEvaluationCalls() : Condition("TrueAfterThreeEvaluationCalls"){};

  void onInit() final { m_evaluation_calls = 0; }

  bool evaluate() final {
    if (m_evaluation_calls >= 3) {
      return true;
    }
    m_evaluation_calls++;
    return false;
  };

 private:
  unsigned int m_evaluation_calls{0};
};

// Test if execution stops after condition is met
TEST(StopAtNodeNodeFixture, stop_at_condition_met) {
  /// -------- TEST SETUP --------------
  // └─ [Decorator::StopAt[TrueAfterThreeEvaluationCalls]]
  //     └─ [Action::AnalyseNodeAlwaysRunning]
  DecoratorNode::Ptr stop_at_decorator =
      std::make_shared<StopAtNode>(std::make_unique<TrueAfterThreeEvaluationCalls>());
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AlwaysRunning>();
  stop_at_decorator->setChild(dummy_node);

  // Initialise the whole tree and call onInit of root node
  stop_at_decorator->distributeData();
  stop_at_decorator->onInit();

  // Tick tree three times - after that the StopAtNode decorator should finish the child behavior.
  EXPECT_EQ(dummy_node->isInitialised(), true);
  EXPECT_EQ(stop_at_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(stop_at_decorator->executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(stop_at_decorator->executeTick(), NodeStatus::kRunning);
  // Finish
  EXPECT_EQ(stop_at_decorator->executeTick(), NodeStatus::kSuccess);

  // Just double check if child behavior was terminated correctly
  stop_at_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

// Test if finishes before until condition is met
TEST(StopAtNodeNodeFixture, finished_before_condition_met) {
  /// -------- TEST SETUP --------------
  // └─ [Decorator::StopAt[TrueAfterThreeEvaluationCalls]]
  //     └─ [Action::AnalyseNode]
  DecoratorNode::Ptr stop_at_decorator =
      std::make_shared<StopAtNode>(std::make_unique<TrueAfterThreeEvaluationCalls>());
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);
  stop_at_decorator->setChild(dummy_node);

  // Initialise the whole tree and call onInit of root node
  stop_at_decorator->distributeData();
  stop_at_decorator->onInit();

  // Tick tree three times - after that the StopAtNode decorator should finish the child behavior.
  EXPECT_EQ(stop_at_decorator->executeTick(), NodeStatus::kRunning);
  // Finish
  EXPECT_EQ(stop_at_decorator->executeTick(), NodeStatus::kSuccess);

  // Just double check if child behavior was terminated correctly
  stop_at_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

}  // namespace yase
