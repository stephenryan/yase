/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/composite/parallel_node.h"
#include "agnostic_behavior_tree/utils/tree_analyzer.h"

#include <gtest/gtest.h>
#include <memory>

namespace yase {

// TEST(TreeAnalyzerTest, get_failing_node) {
//  // test tree with failing nodes
//  CompositeNode::Ptr test_tree_root = std::make_shared<ParallelNode>();
//  test_tree_root->addChild(std::make_shared<AnalyseNode>(3));
//  test_tree_root->addChild(std::make_shared<AlwaysFailure>());
//  test_tree_root->addChild(std::make_shared<AlwaysFailure>());
//  CompositeNode::Ptr sub_tree = std::make_shared<ParallelNode>();
//  sub_tree->addChild(std::make_shared<AlwaysSuccess>());
//  sub_tree->addChild(std::make_shared<AlwaysFailure>());
//  // append the subtree to test_tree_root
//  test_tree_root->addChild(sub_tree);
//
//  // Execute once
//  test_tree_root->executeTick();
//
//  // Result:
//  //└── FAILURE - [Composite::Parallel::Unnamed]
//  //    ├── RUNNING - [Action::AnalyseNode] - Ticked 1 of 3 times
//  //    ├── FAILURE - [Action::AlwaysFailure] -  Returned FAILURE
//  //    ├── IDLE - [Action::AlwaysFailure]
//  //    └── IDLE - [Composite::Parallel::Unnamed]
//  //        ├── IDLE - [Action::AlwaysSuccess]
//  //        └── IDLE - [Action::AlwaysFailure]
//
//  // Expect 2 failing nodes
//  const std::vector<BehaviorNode::Ptr> failing_nodes = getAllFailingNodes(test_tree_root);
//  EXPECT_EQ(2, failing_nodes.size());
//}

TEST(TreeAnalyzerTest, count_nodes) {
  // parallel with three child nodes
  CompositeNode::Ptr test_tree = std::make_shared<ParallelNode>();
  test_tree->addChild(std::make_shared<AnalyseNode>(1));
  test_tree->addChild(std::make_shared<AnalyseNode>(2));
  test_tree->addChild(std::make_shared<AnalyseNode>(3));

  EXPECT_EQ(4, countNodes(*test_tree));
}

}  // namespace yase
