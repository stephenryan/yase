/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/composite/parallel_node.h"
#include "agnostic_behavior_tree/decorator/repeat_node.h"
#include "agnostic_behavior_tree/utils/manipulators.h"

#include <gtest/gtest.h>

namespace yase {

TEST(ManipulatorTest, decorator_test_detach_child) {
  // setChild and detachChild
  RepeatNTimesNode repeat_test_node{0};
  std::shared_ptr<AnalyseNode> analyse_node = std::make_shared<AlwaysRunning>();

  EXPECT_FALSE(repeat_test_node.hasChild());
  EXPECT_FALSE(analyse_node->hasParent());
  repeat_test_node.setChild(analyse_node);
  EXPECT_TRUE(repeat_test_node.hasChild());
  EXPECT_TRUE(repeat_test_node.child().hasParent());
  EXPECT_TRUE(analyse_node->hasParent());

  repeat_test_node.onInit();
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);
  EXPECT_EQ(repeat_test_node.executeTick(), NodeStatus::kRunning);

  auto detached_node = manipulator(repeat_test_node).detachChild();
  EXPECT_FALSE(repeat_test_node.hasChild());
  EXPECT_FALSE(detached_node->hasParent());
  EXPECT_FALSE(analyse_node->hasParent());
  EXPECT_EQ(detached_node.get(), analyse_node.get());

  manipulator(repeat_test_node).setChild(std::move(detached_node));
  EXPECT_TRUE(manipulator(repeat_test_node).hasChild());
  EXPECT_TRUE(analyse_node->hasParent());
  analyse_node.reset();
  EXPECT_TRUE(repeat_test_node.child().hasParent());
}

TEST(ManipulatorTest, composite_test_detach_insert_replace_child) {
  // insert, replace and detachChild
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>();
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  EXPECT_EQ(test_parallel_node->childrenCount(), 1);
  EXPECT_TRUE(test_parallel_node->child(0).hasParent());
  EXPECT_ANY_THROW(manipulator(test_parallel_node).insertChild(std::make_shared<AnalyseNode>(2), 20));
  EXPECT_NO_THROW(manipulator(test_parallel_node).insertChild(std::make_shared<AnalyseNode>(2), 0));
  EXPECT_EQ(test_parallel_node->childrenCount(), 2);
  EXPECT_TRUE(test_parallel_node->child(0).hasParent());

  auto analyse_node = std::make_shared<AnalyseNode>(3);
  EXPECT_FALSE(analyse_node->hasParent());
  manipulator(test_parallel_node).addChild(analyse_node);
  EXPECT_TRUE(analyse_node->hasParent());
  EXPECT_EQ(test_parallel_node->childrenCount(), 3);

  EXPECT_ANY_THROW(manipulator(test_parallel_node).detachChild(10));
  auto detached_node = manipulator(test_parallel_node).detachChild(2);
  EXPECT_EQ(test_parallel_node->childrenCount(), 2);
  EXPECT_FALSE(analyse_node->hasParent());
  EXPECT_FALSE(detached_node->hasParent());
  EXPECT_EQ(detached_node.get(), analyse_node.get());

  EXPECT_ANY_THROW(manipulator(test_parallel_node).replaceChild(analyse_node, 10));
  auto replaced_node = manipulator(test_parallel_node).replaceChild(analyse_node, 0);
  EXPECT_EQ(test_parallel_node->childrenCount(), 2);
  EXPECT_TRUE(manipulator(test_parallel_node).hasChild(1));
  EXPECT_TRUE(analyse_node->hasParent());
  EXPECT_FALSE(replaced_node->hasParent());
}


}  // namespace yase
