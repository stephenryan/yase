FROM ubuntu:20.04
# Required since otherwise the container build will be stuck when installing cmake (cmake as a nonesense question, which shall be ignored)
ARG DEBIAN_FRONTEND=noninteractive

# Get essentials for dev environment
RUN apt-get -y update
RUN apt-get -y install build-essential
RUN apt-get -y install cmake
RUN apt-get -y install clang-format
RUN apt-get -y install ccache
RUN apt-get -y install curl

# Install Bazel
ARG BAZELISK_VERSION=v1.10.1
ARG BAZELISK_DOWNLOAD_SHA=4cb534c52cdd47a6223d4596d530e7c9c785438ab3b0a49ff347e991c210b2cd
RUN curl -fSsL -o /usr/local/bin/bazel https://github.com/bazelbuild/bazelisk/releases/download/${BAZELISK_VERSION}/bazelisk-linux-amd64 \
    && echo "${BAZELISK_DOWNLOAD_SHA} */usr/local/bin/bazel" | sha256sum --check - \
    && chmod 0755 /usr/local/bin/bazel

# Get and build gtest 
RUN apt-get -y install libgtest-dev