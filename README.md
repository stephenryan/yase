# The Yase package

## About Yase:
The **Yase (Yet Agnostic Scenario Engine / Another Scenario Engine)** package is a simulator agnostic scenario/simulation engine in the AD/ADAS context to develop and test intelligent vehicles.
The Yase eco system allows to create customized simulation solutions, which can be adapted easily to the system complexity by adapting extensions for simulators and scenario languages where needed.

The architecture is hereby orientated on programming language compilers. 
Within these compilers, programming languages are translated into an intermediate representation by a front-end. 
From here, several back-ends can translate the intermediate representation into different target architectures.
!["Example of LLVM compiler"](doc/figures/compiler_example.png?raw=true)

This modular and flexible architecture is used for the Yase eco system. 
The basis is the `agnostic_behavior_tree` in the middle-end (also refereed as the simulation kernel). 
Furthermore, it contains multiple other agnostic packages.
Around this middle-end the eco system with extensions can be build, which are agnostic of the underlying simulator as well as of the used scenario input file format.
This middle-end can be filled by different front-ends, compiling in different scenario formats. 
On the other side, back-ends can connect the middle-end with different simulators or simulator feature subsets.
The following figure shows a **potential** expansion with potential simulation back-ends/ scenario formats front-ends.
!["Example for a potential Yase setup"](doc/figures/scenario_compiler.png?raw=true)


## Current Yase content:

The current Yase project consist of one middle-end package.
Please follow the following issues to track the current publishing process for the [`agnostic_type_system` (ATS)](https://gitlab.eclipse.org/eclipse/simopenpass/yase/-/issues/22) and the [OpenSCENARIO2.0 frontend](https://gitlab.eclipse.org/eclipse/simopenpass/yase/-/issues/23).
The implementation of the OpenSCENARIO1.x implementation for OpenPASS can be found [here](https://gitlab.eclipse.org/eclipse/simopenpass/openscenario1_engine).

**Middle-end**
* The **agnostic_behavior_tree** package with an agnostic behavior tree implementation. It allows to build up any scenario behavior with the help self implemented actions.


## How to start:

To play around with the project, one can use the containerized setup.
It contains all required dependencies.
Open the project with docker or [vscode](https://code.visualstudio.com/docs/remote/containers). 

Once the container started, it is possible to build the available packages and run their tests either via cmake or bazel.

Via cmake:
``` shell
mkdir build && cd build
cmake ./../middle_end/agnostic_behavior_tree && make && ./agnostic_behavior_tree_test
```

Via bazel:
``` shell
bazel build //middle_end/agnostic_behavior_tree/...
bazel test --test_output=all //middle_end/agnostic_behavior_tree/...
```
